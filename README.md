# SuiteScript by Example

![project status: active](https://img.shields.io/badge/project_status-active-forestgreen?style=for-the-badge)
[![code style: standard](https://img.shields.io/badge/code_style-standard-forestgreen.svg?style=for-the-badge&logo=standardjs "Standard JavaScript style guide")](https://standardjs.com/)
![suitescript version: 2.1](https://img.shields.io/badge/suitescript_version-2.1-cadetblue?style=for-the-badge)

This repository contains free code samples from
the ["SuiteScript by Example" series](https://stoic.software/cookbooks/) of
cookbooks written
by [Eric T Grubaugh](https://www.linkedin.com/in/erictgrubaugh/). Find the
source code in the
[examples](https://gitlab.com/stoicsoftware/suitescript-by-example/-/tree/main/examples)
directory.

In order to help you learn, remember, and ultimately master the fundamental
concepts of NetSuite Development, these SuiteScript Cookbooks have been created
as real-world, example-focused references for SuiteScript 2.1.

Each Cookbook contains between 6 and 10 examples to help you learn and apply a
particular module or aspect of SuiteScript development. One free example from
each Cookbook is included in this repository.

## Support

If you have questions or suggestions, follow the guidelines in
[CONTRIBUTING.md](https://gitlab.com/stoicsoftware/suitescript-by-example/-/blob/main/CONTRIBUTING.md).

## Authors and acknowledgment

* [Eric T Grubaugh](https://www.linkedin.com/in/erictgrubaugh/) <[eric@stoic.software](mailto:eric@stoic.software)> (https://stoic.software/)

## License

The code is here to help you learn. In general, you may use these code samples
in your own code. You do not need to contact me unless you are
reproducing or redistributing large portions of the code.
I appreciate, but do not require, attribution. An attribution
usually includes the title, author, and publisher:

> "SuiteScript by Example, by Eric T Grubaugh (Stoic Software, LLC).
> Copyright 2024 Stoic Software, LLC."

See
[LICENSE](https://gitlab.com/stoicsoftware/suitescript-by-example/-/blob/main/LICENSE)
file.

