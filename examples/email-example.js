/**
 * Sample code from "Sending Email with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of books by Eric T Grubaugh
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 *
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
require(['N/email', 'N/render'], (email, render) => {
  const template = render.mergeEmail({
    templateId: 17,
    entity: {
      type: 'employee',
      id: 19
    }
  })

  email.send({
    author: -5,
    recipients: [19],
    subject: template.subject,
    body: template.body
  })
})
