/**
 * Sample code from "Rendering PDFs with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of books by Eric T Grubaugh
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 *
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
define(['N/file', 'N/render'], (file, render) => {
  /**
   * Because N/render is only available server-side, you will need a Suitelet
   * or other server-side entry point to test this function.
   *
   * This is explained in detail in the cookbook.
   */
  const renderPdf = (response) => {
    const pdf = render.transaction({
      entityId: 1280,
      formId: 71
    })

    response.writeFile({ file: pdf, isInline: false })
  }

  return { renderPdf }
})
