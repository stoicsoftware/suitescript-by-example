/**
 * Sample code from "Managing Files with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of books by Eric T Grubaugh
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 *
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
define(['N/file'], (f) => {
  const WeatherData = [
    { date: '01/01/2020', high: 51, low: 19 },
    { date: '01/02/2020', high: 45, low: 27 },
    { date: '01/03/2020', high: 43, low: 20 },
    { date: '01/04/2020', high: 55, low: 22 },
    { date: '01/05/2020', high: 41, low: 26 },
    { date: '01/06/2020', high: 43, low: 30 },
    { date: '01/07/2020', high: 57, low: 31 },
    { date: '01/08/2020', high: 55, low: 23 },
    { date: '01/09/2020', high: 42, low: 26 },
    { date: '01/10/2020', high: 31, low: 13 }
  ]

  /**
   * Because N/file is only available server-side, you will need a Suitelet
   * or other server-side entry point to test this function.
   *
   * This is explained in detail in the cookbook.
   */
  const readFile = (response) => {
    const weatherFile = f.create({
      name: 'weather.csv',
      fileType: f.Type.CSV,
      description: 'Stream data to a file, line by line',
      folder: -15
    })

    const lines = WeatherData.map((w) =>
      [w.date, w.low, w.high].join(',')
    )

    lines.forEach((w) => {
      weatherFile.appendLine({ value: w })
    })

    weatherFile.save()

    response.write({ output: weatherFile.getContents() })
  }

  return { readFile }
})
