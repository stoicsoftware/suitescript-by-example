/**
 * Sample code from "Advanced Searching with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of books by Eric T Grubaugh
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 *
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
require(['N/search'], (s) => {
  const employeeSearch = s.create({
    type: s.Type.EMPLOYEE,
    filters: [
      ['isinactive', s.Operator.IS, 'F']
    ],
    columns: [
      { name: 'formulatext', formula: "{firstname} || ' ' || {lastname}" }
    ]
  })

  const timeSearch = s.create({
    type: s.Type.TIME_BILL,
    filters: [
      ['date', s.Operator.WITHIN, 'thisBusinessWeek'], 'and',
      ['type', s.Operator.ANYOF, 'A'] // 'A' => 'Actual Time'
    ],
    columns: [
      { name: 'employee', summary: s.Summary.GROUP }
    ]
  })

  // Display result counts from each search
  console.log(`# Employees = ${employeeSearch.runPaged().count}`)
  console.log(`# Employees with Entries = ${timeSearch.runPaged().count}`)

  // Assume there are less than 1,000 Employees
  const employees = employeeSearch.run().getRange({ start: 0, end: 1000 })
  const timeEntries = timeSearch.run().getRange({ start: 0, end: 1000 })

  // Does the given employee have at least one Time Entry in entries Array?
  const employeeHasEntry = (entries, employee) =>
    entries.some((entry) => {
      let entryEmployee = entry.getValue({
        name: 'employee',
        summary: s.Summary.GROUP
      })
      return (+entryEmployee === +employee.id)
    })

  // Returns a copy of employees Array but removes any Employees
  // that *do* have Time Entries in entries Array
  const findEmployeesWithNoEntries = (employees, entries) =>
    employees.filter((employee) => !employeeHasEntry(entries, employee))

  const printEmployeeName = (result) =>
    console.log(result.getValue({ name: 'formulatext' }))

  const employeesWithNoEntries = findEmployeesWithNoEntries(
    employees,
    timeEntries
  )

  console.log(`# Employees With No Entries = ${employeesWithNoEntries.length}`)
  employeesWithNoEntries.forEach(printEmployeeName)
})
