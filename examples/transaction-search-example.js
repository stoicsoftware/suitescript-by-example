/**
 * Sample code from "Transaction Searches with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of cookbooks by Eric T Grubaugh.
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
require(['N/search'], (s) => {
  const printOrder = (result) => {
    const item = result.getValue({ name: 'displayname' })
    const onHand = result.getValue({ name: 'locationquantityonhand' })
    const onOrder = result.getValue({ name: 'locationquantityonorder' })
    const backOrder = result.getValue({ name: 'locationquantitybackordered' })

    console.group(item)
    console.log(`On Hand: ${onHand}`)
    console.log(`On Order: ${onOrder}`)
    console.log(`Backordered: ${backOrder}`)
    console.groupEnd()

    return true
  }

  s.create({
    type: s.Type.INVENTORY_ITEM,
    filters: [
      ['type', s.Operator.IS, 'InvtPart'], 'and',
      ['inventorylocation', s.Operator.ANYOF, ['4']], 'and',
      ['locationquantityonhand', s.Operator.ISNOTEMPTY, '']
    ],
    columns: [
      'displayname',
      'locationquantitybackordered',
      'locationquantityonhand',
      'locationquantityonorder'
    ]
  }).run().getRange({ start: 0, end: 10 }).forEach(printOrder)
})
