/**
 * Sample code from "Basic Searching with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of cookbooks by Eric T Grubaugh.
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
require(['N/search'], (s) => {
  const findProblemCustomersByRep = (salesRepId) =>
    s.create({
      type: s.Type.CUSTOMER,
      filters: [
        ['salesrep', s.Operator.ANYOF, salesRepId], 'and',
        [
          ['overduebalance', s.Operator.GREATERTHAN, 0], 'or',
          ['credithold', s.Operator.ANYOF, 'ON']
        ]
      ],
      columns: ['entityid', 'email']
    }).run()

  const printCustomerName = (result) => {
    console.log(result.getValue({ name: 'entityid' }))
    return true
  }

  findProblemCustomersByRep('17').each(printCustomerName)
})
