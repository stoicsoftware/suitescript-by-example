/**
 * Sample code from "Basic Optimization with SuiteScript 2.1", 
 * part of the "SuiteScript by Example" series of books by Eric T Grubaugh
 * 
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 * 
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 * 
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */

// Turn this:

const rec = record.load({
  type: record.Type.EMPLOYEE,
  id: '123'
})
rec.setValue({
  fieldId: 'firstname',
  value: 'Eric'
})
rec.setValue({
  fieldId: 'middlename',
  value: 'T'
})
rec.setValue({
  fieldId: 'lastname',
  value: 'Grubaugh'
})
rec.setValue({
  fieldId: 'title',
  value: 'SuiteScript Strategist'
})
rec.save()

// into this:

record.submitFields({
  type: record.Type.EMPLOYEE,
  id: '123',
  values: {
    firstname: 'Eric',
    middlename: 'T',
    lastname: 'Grubaugh',
    title: 'NetSuite Developer Advisor'
  }
})
