/**
 * Sample code from "Basic Querying with SuiteScript 2.1",
 * part of the "SuiteScript by Example" series of books by Eric T Grubaugh
 *
 * Find the entire "SuiteScript by Example" series at
 * https://suitescriptbyexample.com/
 *
 * @copyright 2024 Stoic Software, LLC. All rights reserved. No license granted.
 *
 * @author Eric T Grubaugh <eric@stoic.software> (https://stoic.software/)
 */
require(['N/query'], (q) => {
  const findCustomersByRep = (salesRepId) => {
    const customerQuery = q.create({
      type: q.Type.CUSTOMER,
      columns: [
        { fieldId: 'email' },
        { fieldId: 'entityid', alias: 'name' }
      ]
    })

    customerQuery.condition = customerQuery.and(
      customerQuery.createCondition({
        fieldId: 'isperson',
        operator: q.Operator.IS,
        values: true
      }),
      customerQuery.createCondition({
        fieldId: 'salesrep',
        operator: q.Operator.ANY_OF,
        values: salesRepId
      })
    )

    return customerQuery.run().asMappedResults()
  }

  const printCustomerNameAndEmail = (result) => {
    console.log(`${result.name} - ${result.email}`)
  }

  // Replace -5 with the internal ID of any Sales Rep ID in your account
  findCustomersByRep(-5).forEach(printCustomerNameAndEmail)
})
