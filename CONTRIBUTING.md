<!-- omit in toc -->
# Contributing to SuiteScript by Example

First off, thanks for taking the time to contribute! ❤️

All types of contributions are encouraged and valued. See the [Table of Contents](#table-of-contents) for different ways to help and details about how this project handles them. Please make sure to read the relevant section before making your contribution. It will make it a lot easier for us maintainers and smooth out the experience for all involved. The community looks forward to your contributions. 🎉

> And if you like the project, but just don't have time to contribute, that's fine. There are other easy ways to support the project and show your appreciation, which we would also be very happy about:
> - Star the project
> - Refer this project in your project's readme
> - Mention the project at local meetups and tell your friends/colleagues

<!-- omit in toc -->
## Table of Contents

- [Code of Conduct](#code-of-conduct)
- [I Have a Question](#i-have-a-question)
- [I Want To Contribute](#i-want-to-contribute)
  - [Suggesting Enhancements](#suggesting-enhancements)


## Code of Conduct

This project and everyone participating in it is governed by the
[SuiteScript by Example Code of Conduct](https://gitlab.com/stoicsoftware/suitescript-by-example/blob/master/CODE_OF_CONDUCT.md).
By participating, you are expected to uphold this code. Please report unacceptable behavior
to [GitLab](https://docs.gitlab.com/ee/user/report_abuse.html).


## I Have a Question

Before you ask a question, it is best to search for existing [Issues](https://gitlab.com/stoicsoftware/suitescript-by-example//issues) that might help you. In case you have found a suitable issue and still need clarification, you can add your question in the Issue's comments. It is also advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification, we recommend the following:

- Open an [Issue](https://gitlab.com/stoicsoftware/suitescript-by-example//issues/new).
- Tag it as a `question`.
- Provide as much context as you can about your question.

## I Want To Contribute

> ### Legal Notice <!-- omit in toc -->
> When contributing to this project, you must agree that you have authored 100% of the content, that you have the necessary rights to the content and that the content you contribute may be provided under the project license.

### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for SuiteScript by Example, **including completely new examples and minor improvements to existing examples**. Following these guidelines will help maintainers and the community to understand your suggestion and find related suggestions.

<!-- omit in toc -->
#### Before Submitting an Enhancement

- Make sure that you are using the latest version.
- Perform a [search](https://gitlab.com/stoicsoftware/suitescript-by-example//issues) to see if the example or change has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project. It's up to you to make a strong case to convince the project's developers of the merits of your request. Keep in mind that we want examples that will be useful to the majority of NetSuite developers. If you're just targeting a minority of users, or submitting your own personal tasks, that is not what this repository is for.

<!-- omit in toc -->
#### How Do I Submit a Good Enhancement Suggestion?

Enhancement suggestions are tracked as [Issues](https://gitlab.com/stoicsoftware/suitescript-by-example//issues).

- Use a **clear and descriptive title** for the issue to identify the suggestion.
- Provide a **clear description of the suggested enhancement** in as many details as possible.
- **Explain why this enhancement would be useful** to most SuiteScript developers. You may also want to point out other references which could serve as inspiration.

## Attribution
This guide is based on the **contributing-gen**. [Make your own](https://github.com/bttger/contributing-gen)!
